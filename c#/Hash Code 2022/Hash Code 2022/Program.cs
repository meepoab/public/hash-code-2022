﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace Hash_Code_2022
{
    class Program
    {
        static void Main(string[] args)
        {
            new Thread(() => run("a")).Start();
            new Thread(() => run("b")).Start();
            new Thread(() => run("c")).Start();
            new Thread(() => run("d")).Start();
            new Thread(() => run("e")).Start();
            new Thread(() => run("f")).Start();
        }

        private static void run(string problem)
        {
            var ContributorsBySkill = new Dictionary<string, List<Contributor>>();
            var Contributors = new List<Contributor>();
            var Projects = new List<Project>();
            string[] rows = File.ReadAllLines("files/" + problem + ".txt");
            string[] headerRow = rows[0].Split(" ");
            int contributorCount = int.Parse(headerRow[0]);
            int projectCount = int.Parse(headerRow[1]);
            int index = 1;
            for (int i = 0; i < contributorCount; ++i)
            {
                var contributorRow = rows[index++].Split(" ");
                var cc = new Contributor
                {
                    index = i,
                    Name = contributorRow[0]
                };
                int n = int.Parse(contributorRow[1]);
                for (int j = 0; j < n; ++j)
                {
                    var skillRow = rows[index++].Split(" ");
                    var skill = int.Parse(skillRow[1]);
                    cc.skills.Add(skillRow[0], skill);
                    cc.totalSkill += skill;
                }
                Contributors.Add(cc);
            }
            for (int i = 0; i < projectCount; ++i)
            {
                var project = new Project(rows[index++]);
                for (int j = 0; j < project.NumberOfRoles; ++j)
                {
                    var projectRow = rows[index++].Split(" ");
                    project.Skills.Add(projectRow[0]);
                    project.Req.Add(int.Parse(projectRow[1]));
                }
                Projects.Add(project);
            }
            Projects = Projects.OrderByDescending(p => p.Score).ToList();

            foreach (var contributor in Contributors)
            {
                foreach (var skill in contributor.skills.Keys)
                {
                    if (!ContributorsBySkill.ContainsKey(skill))
                    {
                        ContributorsBySkill.Add(skill, new List<Contributor>());
                    }
                    ContributorsBySkill[skill].Add(contributor);
                }
            }

            Projects = Projects
                .Where(p => p.Skills.All(s => ContributorsBySkill.ContainsKey(s)))
                .ToList();

            var pendingProjects = new List<CompletedProject>();
            var completedProjects = new List<CompletedProject>();

            for (int t = 0; ; )
            {
                for (int i = 0; i < pendingProjects.Count; ++i)
                {
                    var p = pendingProjects[i];
                    if (t == p.CompletedAt)
                    {
                        pendingProjects.Remove(p);
                        completedProjects.Add(p);
                        --i;
                        for (int j = 0; j < p.contributors.Count; ++j)
                        {
                            var c = p.contributors[j];
                            var skillName = p.project.Skills[j];
                            if (!c.skills.ContainsKey(skillName))
                            {
                                c.skills.Add(skillName, 1);
                            }
                            else if (c.skills[skillName] <= p.project.Req[j])
                            {
                                ++c.skills[skillName];
                            }
                            c.Working = false;
                        }
                    }
                }

                var byScore = Projects.OrderByDescending(p => GetProjectScore(t, p)).ToList();
                if (byScore.Count > 0 && byScore[0].completedScore > 0)
                {
                    foreach (var p in byScore)
                    {
                        if (CanStart(t, pendingProjects, p, ContributorsBySkill, Contributors))
                        {
                            Projects.Remove(p);
                        }
                    }
                }
                if (pendingProjects.Count == 0)
                {
                    break;
                }
                t = pendingProjects
                    .Select(p => p.CompletedAt)
                    .Min();
            }

            writeOutput(problem, completedProjects);
        }

        private static bool CanStart(int t, List<CompletedProject> pendingProjects, Project project, Dictionary<string, List<Contributor>> ContributorsBySkill, List<Contributor> Contributors)
        {
            var w = new List<Contributor>();
            var mentorSkills = new Dictionary<string, int>();
            var usedIndexes = new HashSet<int>();
            var ww = new Contributor[project.Skills.Count];
            var levelUps = new List<Contributor>();
            while (usedIndexes.Count < project.Skills.Count)
            {
                int min = -1;
                int index = -1;
                for (int i = 0; i < project.Skills.Count; ++i)
                {
                    if (usedIndexes.Contains(i))
                    {
                        continue;
                    }
                    var pSkill = project.Skills[i];
                    var num = ContributorsBySkill[pSkill]
                        .Where(c => !c.Working)
                        .Where(c => !w.Contains(c))
                        .Where(c => c.skills[pSkill] >= project.Req[i] || (mentorSkills.GetValueOrDefault(pSkill, 0) >= project.Req[i] && c.skills[pSkill] == project.Req[i] - 1))
                        .OrderBy(c => c.skills[pSkill] * 10000 + c.totalSkill)
                        .Count();
                    if (num == 0)
                    {
                        if (mentorSkills.ContainsKey(pSkill) && project.Req[i] == 1)
                        {
                            num = 5;
                        }
                    }
                    if (num > 0 && (min == -1 || num <= min))
                    {
                        min = num;
                        index = i;
                    }
                }
                if (min == -1)
                {
                    return false;
                }
                var skill = project.Skills[index];
                var cc = ContributorsBySkill[skill]
                        .Where(c => !c.Working)
                        .Where(c => !w.Contains(c))
                        .Where(c => c.skills[skill] >= project.Req[index] || (mentorSkills.GetValueOrDefault(skill, 0) >= project.Req[index] && c.skills[skill] == project.Req[index] - 1))
                        .OrderBy(c => c.skills[skill] * 10000 + c.totalSkill)
                        .FirstOrDefault();
                if (cc == null)
                {
                    if (mentorSkills.ContainsKey(skill) && project.Req[index] == 1)
                    {
                        cc = Contributors
                            .Where(c => !c.Working)
                            .Where(c => !w.Contains(c))
                            .FirstOrDefault();
                    }
                    if (cc == null)
                    {
                        return false;
                    }
                }
                foreach (var s in cc.skills)
                {
                    if (!mentorSkills.ContainsKey(s.Key))
                    {
                        mentorSkills.Add(s.Key, s.Value);
                    }
                    else if (mentorSkills[s.Key] < s.Value)
                    {
                        mentorSkills[s.Key] = s.Value;
                    }
                }
                if (!cc.skills.ContainsKey(skill) || cc.skills[skill] == project.Req[index] - 1)
                {
                    levelUps.Add(cc);
                }
                w.Add(cc);
                ww[index] = cc;
                usedIndexes.Add(index);
            }
            pendingProjects.Add(new CompletedProject
            {
                CompletedAt = t + project.DaysRequired,
                project = project,
                contributors = new List<Contributor>(ww),
                score = project.completedScore
            });
            foreach (var c in w)
            {
                c.Working = true;
            }
            return true;
        }

        private static void writeOutput(string problem, List<CompletedProject> completedProjects)
        {
            int totalScore = 0;
            var sb = new StringBuilder();
            sb.Append(completedProjects.Count).Append("\n");
            foreach (var p in completedProjects)
            {
                totalScore += p.score;
                sb.Append(p.project.Name).Append("\n");
                for (int i = 0; i < p.contributors.Count; ++i)
                {
                    if (i > 0)
                    {
                        sb.Append(" ");
                    }
                    sb.Append(p.contributors[i].Name);
                }
                sb.Append("\n");
            }
            File.WriteAllText(problem + "_" + totalScore + ".out", sb.ToString());
            Console.WriteLine("problem " + problem + " finished with score " + totalScore);
        }

        public static double GetProjectScore(int currentDay, Project p)
        {
            var finishedOnDay = currentDay + p.DaysRequired;
            p.completedScore = Math.Max(p.Score + Math.Min(p.BestBeforeDay - finishedOnDay, 0), 0);
            return p.completedScore / (double)(p.DaysRequired*p.NumberOfRoles);
        }
    }

    public class Contributor
    {
        public override string ToString() => Name;

        public int index;
        public string Name;
        public bool Working = false;
        public int totalSkill = 0;
        public Dictionary<string, int> skills = new Dictionary<string, int>();
    }

    public class Project
    {
        public override string ToString() => Name;

        public string Name { get; set; }
        public int DaysRequired { get; set; }
        public int Score { get; set; }
        public int BestBeforeDay { get; set; }
        public int NumberOfRoles { get; set; }
        public List<string> Skills = new List<string>();
        public List<int> Req = new List<int>();
        public int position = 0;
        public int completedScore = 0;

        public Project(string projectLine)
        {
            var split = projectLine.Split(" ");
            Name = split[0];
            DaysRequired = int.Parse(split[1]);
            Score = int.Parse(split[2]);
            BestBeforeDay = int.Parse(split[3]);
            NumberOfRoles = int.Parse(split[4]);
        }
    }

    public class CompletedProject
    {
        public int CompletedAt = -1;
        public Project project;
        public List<Contributor> contributors = new List<Contributor>();
        public int score = 0;
    }
}
