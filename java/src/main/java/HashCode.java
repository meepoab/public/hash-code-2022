import java.io.*;

public class HashCode {

    public static void main(String[] args) {
        run("a");
        /*
        run("b");
        run("c");
        run("d");
        run("e");
        run("f");
         */
    }

    private static void run(String problem) {
        final String inFile = problem + ".txt";

        try {
            final BufferedReader in = new BufferedReader(new InputStreamReader(HashCode.class.getResourceAsStream(inFile)));
            String row = in.readLine();

            final String outFile = problem + ".out";
            createOutfile(problem);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void createOutfile(String problem) throws IOException {
        final BufferedWriter out = new BufferedWriter(new FileWriter(getOutPath(problem)));
        out.flush();
        out.close();
    }

    private static String getOutPath(String problem) {
        var path = HashCode.class.getResource(problem + ".txt").getPath().replace("build", "src/main");
        return path.substring(0, path.length() - 10) + problem + ".out";
    }
}
